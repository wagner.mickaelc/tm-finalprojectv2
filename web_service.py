from pydantic.errors import IntEnumError
from configuration import *

import uvicorn
from fastapi import FastAPI
import requests
from pydantic import BaseModel
from newsapi import NewsApiClient
from random import shuffle
from datetime import datetime

class Item(BaseModel):
    categories: list

# Init
app = FastAPI()
newsapi = NewsApiClient(api_key=API_KEY)

@app.post("/")
def news(body : Item):
    articles = []

    for category in body.categories:
        try:
            all_articles = newsapi.get_everything(q=category,
                                      #from_param='2021-05-21',
                                      from_param = datetime.now().strftime("%Y-%m-%d"),
                                      language='fr',
                                      sort_by='relevancy',
                                      page=1)
            articles = articles + all_articles["articles"]
        except ValueError:
            pass
    for i in range(len(articles)):
        article = articles[i]

    return {"status":200,"articles":articles}

if __name__ == "__main__":
    uvicorn.run(app, host=HOST_NAME, port=HOST_PORT)
