import React, { createContext, useReducer } from "react";
import Reducer from './Reducer'



const Context = createContext([{}, function () { }]);

const Store = ({ children }) => {

    const initialState = {
        posts: [],
        error: null
    };

    const [state, dispatch] = useReducer(Reducer, initialState);
    return (
        <Context.Provider value={[state, dispatch]}>
            {children}
        </Context.Provider>
    )
};

export { Context, Store };