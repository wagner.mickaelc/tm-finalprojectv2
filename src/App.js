// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native

// Dot env pour les keys

import 'react-native-gesture-handler';

import React, { useEffect, useState, useReducer, useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from './screens/HomeScreen';
import RegisterUser from './screens/RegisterUser';
import UpdateUser from './screens/UpdateUser';
import ViewUser from './screens/ViewUser';
import LoginScreen from './screens/LoginScreen';
import BlankScreen from './screens/BlankScreen';
import LocalScreen from './screens/LocalScreen';
import InterestsScreen from './screens/InterestsScreen';
import News from './screens/News';

import { openDatabase } from 'react-native-sqlite-storage';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { Store, Context } from './Store';

var db = openDatabase({ name: 'Database.db' });

const Stack = createStackNavigator();

const App = () => {


  const state = useContext(Context);
  const [localMode, setLocalMode] = useState()
  const [preferences, setPreferences] = useState()

  // Initialition des data base au lancement de l'application.
  useEffect(() => { // Requête / champs / fonction exécutée suite à la requête

    // Initialisation de la base de données si besoin
    db.transaction(function (txn) {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table'", // Récupère le nombre de table existantes.
        [],
        function (res) {
          // Si y'en a 0 :
          if (true /*res.rows.length == 0*/) {
            txn.executeSql('DROP TABLE IF EXISTS preferences', []);
            txn.executeSql('CREATE TABLE IF NOT EXISTS preferences(id INTEGER PRIMARY KEY AUTOINCREMENT, category)', []);
          }
        },
      );
    });



    // Initialisation des settings en cas de besoin
    if (true /*getData("profile").then(response => response == null) == true*/) {
      storeData("profile", { local: false });
    }
    getData("profile").then(response => setLocalMode(JSON.parse(response).local))

    // Initialisation des préférences
    if (localMode == true) {
      db.transaction(function (txn) {
        txn.executeSql("SELECT * FROM preferences", // Récupère le nombre de table existantes.
          [],
          function (res) {
            let array = [];
            var len = res.rows.length;
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              array.push(row.category)
            };
            setPreferences(array);
          })
      })
    }
  }, [state]);

  // Permet de store la configuration de base.
  const storeData = async (key, value) => {
    try {
      const jsonValue = JSON.stringify(value)
      console.log("Value Stored in " + key + " : ", jsonValue)
      await AsyncStorage.setItem("@" + key, jsonValue)
    } catch (e) {
      console.error(e)
    }
  }

  // Permet de récupérer les données.
  const getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem("@" + key)
      if (value !== null) {
        console.log("Exported Data : ", value)
        return (value)
      }
    } catch (e) {
      console.error(e)
    }
  }

  console.log("LocalMode : ", localMode)
  return (<Store>
    < NavigationContainer >


      <Stack.Navigator>

        {/*Temporaire le temps de régler le soucis de UseEffect, ... !*/}

        <Stack.Screen
          name="LocalScreen"
          component={LocalScreen}
          options={{
            title: 'LocalScreen', //Header Title
            headerStyle: {
              backgroundColor: '#fcad03', //Header color
            },
            headerTintColor: '#fff', //Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Header text style
            },
            changeLocalMode: setLocalMode,
          }
          }
        />
        <Stack.Screen
          name="HomeScreen"
          component={HomeScreen}
          options={{
            title: 'Accueil', //Header Title
            headerStyle: {
              backgroundColor: '#fcad03', //Header color
            },
            headerTintColor: '#fff', //Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Header text style
            },
            changeLocalMode: setLocalMode,
          }
          }
        />
        <Stack.Screen
          name="News"
          component={News}
          options={{
            title: 'News', //Header Title
            headerStyle: {
              backgroundColor: '#fcad03', //Header color
            },
            headerTintColor: '#fff', //Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Header text style
            },
            changeLocalMode: setLocalMode,
          }
          }
        />
        <Stack.Screen
          name="InterestsScreen"
          component={InterestsScreen}
          options={{
            title: 'InterestsScreen', //Header Title
            headerStyle: {
              backgroundColor: '#fcad03', //Header color
            },
            headerTintColor: '#fff', //Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Header text style
            },
            changeLocalMode: setLocalMode,
          }
          }
        />
        <Stack.Screen
          name="View"
          component={ViewUser}
          options={{
            title: 'View User', //Header Title
            headerStyle: {
              backgroundColor: '#fcad03', //Header color
            },
            headerTintColor: '#fff', //Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Header text style
            },
          }}
        />
        <Stack.Screen
          name="Register"
          component={RegisterUser}
          options={{
            title: 'Inscription', //Header Title
            headerStyle: {
              backgroundColor: '#fcad03', //Header color
            },
            headerTintColor: '#fff', //Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Header text style
            },
          }}
        />
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{
            title: 'Login', //Header Title
            headerStyle: {
              backgroundColor: '#fcad03', //Header color
            },
            headerTintColor: '#fff', //Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Header text style
            },
          }}
        />
        <Stack.Screen
          name="Update"
          component={UpdateUser}
          options={{
            title: 'Update User', //Header Title
            headerStyle: {
              backgroundColor: '#fcad03', //Header color
            },
            headerTintColor: '#fff', //Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Header text style
            },
          }}
        />
        <Stack.Screen
          name=" "
          component={BlankScreen}
        />
        {/*localMode == false ? (
          <>
            <Stack.Screen
              name="HomeScreen"
              component={HomeScreen}
              options={{
                title: 'Accueil', //Header Title
                headerStyle: {
                  backgroundColor: '#fcad03', //Header color
                },
                headerTintColor: '#fff', //Header text color
                headerTitleStyle: {
                  fontWeight: 'bold', //Header text style
                },
                changeLocalMode: setLocalMode,
              }
              }
            />
            <Stack.Screen
              name="View"
              component={ViewUser}
              options={{
                title: 'View User', //Header Title
                headerStyle: {
                  backgroundColor: '#fcad03', //Header color
                },
                headerTintColor: '#fff', //Header text color
                headerTitleStyle: {
                  fontWeight: 'bold', //Header text style
                },
              }}
            />
            <Stack.Screen
              name="Register"
              component={RegisterUser}
              options={{
                title: 'Inscription', //Header Title
                headerStyle: {
                  backgroundColor: '#fcad03', //Header color
                },
                headerTintColor: '#fff', //Header text color
                headerTitleStyle: {
                  fontWeight: 'bold', //Header text style
                },
              }}
            />
            <Stack.Screen
              name="Login"
              component={LoginScreen}
              options={{
                title: 'Login', //Header Title
                headerStyle: {
                  backgroundColor: '#fcad03', //Header color
                },
                headerTintColor: '#fff', //Header text color
                headerTitleStyle: {
                  fontWeight: 'bold', //Header text style
                },
              }}
            />
          </>
        ) : localMode == true ? (<>
          <Stack.Screen
            name="Update"
            component={UpdateUser}
            options={{
              title: 'Update User', //Header Title
              headerStyle: {
                backgroundColor: '#fcad03', //Header color
              },
              headerTintColor: '#fff', //Header text color
              headerTitleStyle: {
                fontWeight: 'bold', //Header text style
              },
            }}
          />
        </>
        ) : (
          <Stack.Screen
            name=" "
            component={BlankScreen}
          />
        )*/}
      </Stack.Navigator>
    </NavigationContainer >
  </Store >);
};

export default App;
