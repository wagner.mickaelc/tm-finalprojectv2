// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native

import React, { useEffect } from 'react';
import { View, Text, Image, SafeAreaView, StyleSheet } from 'react-native';
import Mybutton from './components/Mybutton';
import Mytext from './components/Mytext';
import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const LocalScreen = ({ navigation }) => {
    useEffect(() => {
        db.transaction(function (txn) {
            txn.executeSql(
                "SELECT name FROM sqlite_master WHERE type='table' AND name='table_user'",
                [],
                function (tx, res) {
                    console.log('item:', res.rows.length);
                    if (res.rows.length == 0) {
                        txn.executeSql('DROP TABLE IF EXISTS table_user', []);
                        txn.executeSql(
                            'CREATE TABLE IF NOT EXISTS table_user(user_id INTEGER PRIMARY KEY AUTOINCREMENT, user_name VARCHAR(20), user_contact INT(10), user_address VARCHAR(255))',
                            [],
                        );
                    }
                },
            );
        });
    }, []);

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <Image style={styles.image_name} source={require("./assets/logoLarge.png")} />
                    <Mybutton
                        title="Afficher les News"
                        customClick={() => navigation.navigate('News')}
                    />
                    <Mybutton
                        title="Centres d'intérêt"
                        customClick={() => navigation.navigate('InterestsScreen')}
                    />
                    {/*<Mybutton
                        title="Page d'Accueil"
                        customClick={() => navigation.navigate('HomeScreen')}
                    />*/}
                </View>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },

    image: {
        marginBottom: 40,
    },

    image_name: {
        width: "74%",
        resizeMode: "stretch",
        padding: 10,
        marginTop: 16,
        marginLeft: "13%",
        marginRight: "13%",
    },
    inputView: {
        backgroundColor: "#FFDED7",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,

        alignItems: "center",
    },

    forgot_button: {
        height: 30,
        marginBottom: 30,
    },

    Button: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: "#FF6B4C",
    },
});

export default LocalScreen;
