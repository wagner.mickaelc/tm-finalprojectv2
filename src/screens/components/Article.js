// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native
// Custom Button

import React from 'react';
import { Text, StyleSheet, Linking, Share, Button } from 'react-native';
import Mytext from './Mytext';
import Mybutton from './Mybutton';

const Article = (props) => {
    return (
        <>
            <Mytext
                style={styles.text}
                text={props.title}
            />
            {/*<Mytext
                style={styles.text}
                text={"Source : " + props.source}
            />*/}
            <Mytext
                style={styles.text}
                text={"Description : " + props.description}
            />
            <Text style={styles.lien}
                onPress={() => Linking.openURL(props.url)}>
                Lien vers l'article
            </Text>
            {/*<Mytext
                style={styles.text}
                text={"Image Url : " + props.urlToImage}
            />*/}
            <Mytext
                style={styles.text}
                text={props.author + " " + props.publishedAt}
            />

            <Mybutton
                customClick={() => {
                    Share.share({ message: props.url });
                }}
                title="Partager"
                color="#841584"
            />
        </>
    )
};

const styles = StyleSheet.create({
    button: {
        alignItems: 'center',
        backgroundColor: '#f05555',
        color: '#ffffff',
        width: "74%",
        padding: 10,
        marginTop: 16,
        marginLeft: "13%",
        marginRight: "13%",
    },
    text: {
        color: '#ffffff',
    },
    lien: {
        color: '#111825',
        fontSize: 18,
        marginTop: 16,
        marginLeft: 35,
        marginRight: 35,
    }
});

export default Article;
