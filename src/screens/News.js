// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native
// Screen to register the user

import React, { useState } from 'react';
import {
    View,
    ScrollView,
    KeyboardAvoidingView,
    Alert,
    SafeAreaView,
    Text,
} from 'react-native';
import Mytextinput from './components/Mytextinput';
import Mybutton from './components/Mybutton';
import Article from './components/Article';
import { openDatabase } from 'react-native-sqlite-storage';
import { useEffect } from 'react';

var db = openDatabase({ name: 'UserDatabase.db' });

const News = () => {
    let [index, setIndex] = useState(0);
    let [categories, setCategories] = useState(['bitcoin', 'informatique'])
    let [articles, setArticles] = useState([{}])
    let [data, setData] = useState([])
    useEffect(() => { // Requête / champs / fonction exécutée suite à la requête
        // Initialisation de la base de données si besoin
        const getData = () => {
            temp = []

            db.transaction(
                tx => {
                    tx.executeSql('SELECT * FROM preferences', [], (tx, res) => {
                        for (let i = 0; i < res.rows.length; i++) {
                            temp.push(res.rows.item(i)["category"])
                            console.log(res.rows.item(i)["category"]);
                        }
                        setData(temp)
                    }
                    );
                }
            );
        }
        fetch('http://192.168.0.10:8000', {
            method: 'POST',
            //body: JSON.stringify({ "categories": data })
            body: JSON.stringify({ "categories": ["bitcoin", "economie"] })
        }).then((response) => response.json()).then((json) => {
            console.log(json)
            setArticles(json["articles"]);
        })

    }, []);

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <ScrollView keyboardShouldPersistTaps="handled">
                        <Article
                            author={articles[index].author}
                            /*source={articles[index].source}*/
                            title={articles[index].title}
                            description={articles[index].description}
                            url={articles[index].url}
                            urlToImage={articles[index].urlToImage}
                            publishedAt={articles[index].publishedAt}
                        />
                        {index > 0 ?
                            < Mybutton
                                title="Article précédent"
                                customClick={() => setIndex(index - 1)}
                            />
                            : <></>}
                        {index < articles.length ?
                            <Mybutton
                                title="Article Suivant"
                                customClick={() => setIndex(index + 1)}
                            />
                            : <></>}

                        <KeyboardAvoidingView
                            behavior="padding"
                            style={{ flex: 1, justifyContent: 'space-between' }}>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </View>
            </View>
        </SafeAreaView >
    );
};

export default News;
