// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native


// Local Storage
// Indexed db

import React, { useContext } from 'react';
import { View, Text, Image, SafeAreaView, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage'
import Mybutton from './components/Mybutton';
import Mytext from './components/Mytext';
import { openDatabase } from 'react-native-sqlite-storage';
import App from '../App';
import Store, { Context } from "../Store"

const HomeScreen = ({ navigation }) => {
  const [state, dispatch] = useContext(Context);
  /*
    const getData = async () => {
      try {
        const jsonValue = await AsyncStorage.getItem('@profile')
        return jsonValue != null ? JSON.parse(jsonValue) : null;
      } catch (e) {
        // lance une erreur
      }
    }
    const AppHeader = () => {
      getData().then((id) => {
        console.log("id : " + id);
        //return id === ''? <Text>Coucou</Text>: <Text>Pas coucou</Text>;
      });
    }
    storeData({ "test": "Test" })
    AppHeader()
   
    AsyncStorage.setItem("mykey", "myValue")*/

  /*const recup = async () => AsyncStorage.getItem("mykey")
  recup().then((id) => console.log(id))*/
  // Permet de store la configuration de base.

  const storeData = async (key, value) => {
    try {
      const jsonValue = JSON.stringify(value)
      console.log("Value Stored in " + key + " : ", jsonValue)
      await AsyncStorage.setItem("@" + key, jsonValue)
    } catch (e) {
      console.error(e)
    }
  }

  // Permet de récupérer les données.
  const getData = async (key) => {
    try {
      const value = await AsyncStorage.getItem("@" + key)
      if (value !== null) {
        console.log("Exported Data : ", value)
        return (value)
      }
    } catch (e) {
      console.error(e)
    }
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <Image style={styles.image_name} source={require("./assets/logoLarge.png")} />
          <Mybutton
            title="Inscription"
            customClick={() => navigation.navigate('Register')}
          />
          <Mybutton
            title="Connexion"
            customClick={() => navigation.navigate('Login')}
          />
          <Mybutton
            title="Local Mode"
            customClick={() => navigation.navigate('LocalScreen')}
          />

          {/*
          <Mybutton
            title="Local Mode"
            customClick={() => { storeData("profile", { local: true }).then(() => dispatch({ type: 'SET_POSTS', payload: { local: true } }));/* getData("profile");*//*}}
          />*/}
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  image: {
    marginBottom: 40,
  },

  image_name: {
    width: "74%",
    resizeMode: "stretch",
    padding: 10,
    marginTop: 16,
    marginLeft: "13%",
    marginRight: "13%",
  },
  inputView: {
    backgroundColor: "#FFDED7",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,

    alignItems: "center",
  },

  forgot_button: {
    height: 30,
    marginBottom: 30,
  },

  Button: {
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: "#FF6B4C",
  },
});

export default HomeScreen;
