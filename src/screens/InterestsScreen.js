// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native

import React, { useEffect, useState } from 'react';
import {
  View,
  ScrollView,
  KeyboardAvoidingView,
  Alert,
  SafeAreaView,
  Text,
  StyleSheet,
} from 'react-native';
import Mybutton from './components/Mybutton';
import Mytextinput from './components/Mytextinput';
import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'Database.db' });

const InterestsScreen = ({ navigation }) => {
  let [category, setCategory] = useState('');
  let [data, setData] = useState('En attente d\'une nouvelle catégorie ...');

  const getData = () => {
    temp = []

    db.transaction(
      tx => {
        tx.executeSql('SELECT * FROM preferences', [], (tx, res) => {
          for (let i = 0; i < res.rows.length; i++) {
            temp.push(res.rows.item(i)["category"])
            console.log(res.rows.item(i)["category"]);
          }
          setData(temp)
        }
        );
      }
    );
  }

  let add_category = () => {

    if (!category) {
      alert('Merci de renseigner un centre d\'intérêt avant de valider!');
      return;
    }

    console.log(category);
    db.transaction(function (tx) {
      tx.executeSql(
        'INSERT INTO preferences (id, category) VALUES (NULL,?)',
        [category],
        (tx, results) => {
          console.log('Results', results.rowsAffected);
          if (results.rowsAffected > 0) {
            Alert.alert(
              'Succès !',
              'La catégorie est désormais renseignée !',
              [
                {
                  text: 'Ok',
                  onPress: () => navigation.navigate('InterestsScreen'),
                },
              ],
              { cancelable: false },
            );
          } else alert('L\'enregistrement à échoué.',
            'Veuillez réessayer !');
        },
      );
    });
    console.log("Get Data Start")
    getData()
    console.log("Get Data End")
    console.log("Indev : Ajout Base")
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ flex: 1 }}>
          <ScrollView keyboardShouldPersistTaps="handled">
            <KeyboardAvoidingView
              behavior="padding"
              style={{ flex: 1, justifyContent: 'space-between' }}>
              <Mytextinput
                placeholder="Centre d'intérêt"
                onChangeText={(category) => setCategory(category)}
                style={{ padding: 10 }}
              />
              <Mybutton title={"" + data} customClick={() => null} />
              <Mybutton title="Submit" customClick={add_category} />
            </KeyboardAvoidingView>
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default InterestsScreen;
