// Example: Example of SQLite Database in React Native
// https://aboutreact.com/example-of-sqlite-database-in-react-native
// Screen to register the user

import React, { useState } from 'react';
import {
    View,
    ScrollView,
    KeyboardAvoidingView,
    Alert,
    SafeAreaView,
    Text,
} from 'react-native';
import Mytextinput from './components/Mytextinput';
import Mybutton from './components/Mybutton';
import { openDatabase } from 'react-native-sqlite-storage';

var db = openDatabase({ name: 'UserDatabase.db' });

const LoginScreen = ({ navigation }) => {
    let [pseudo, setPseudo] = useState('');
    let [password, setPassword] = useState('');

    let register_user = () => {
        console.log(pseudo, password);

        if (!pseudo) {
            alert('Merci de renseigner un pseudonyme !');
            return;
        }
        if (!password) {
            alert('Merci de renseigner un mot de passe !');
            return;
        }

        db.transaction(function (tx) {
            tx.executeSql(
                'INSERT INTO table_user (user_name, user_contact, user_address) VALUES (?,?,?)',
                [userName, userContact, userAddress],
                (tx, results) => {
                    console.log('Results', results.rowsAffected);
                    if (results.rowsAffected > 0) {
                        Alert.alert(
                            'Succès !',
                            'Vous êtes désormais connecté !',
                            [
                                {
                                    text: 'Ok',
                                    onPress: () => navigation.navigate('HomeScreen'),
                                },
                            ],
                            { cancelable: false },
                        );
                    } else alert('La connexion a échoué !',
                        'Veuillez réessayer !');
                },
            );
        });
        console.log("Ajout Base : Indev !");
        navigation.navigate('HomeScreen');
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={{ flex: 1 }}>
                    <ScrollView keyboardShouldPersistTaps="handled">
                        <KeyboardAvoidingView
                            behavior="padding"
                            style={{ flex: 1, justifyContent: 'space-between' }}>
                            <Mytextinput
                                placeholder="Pseudo"
                                onChangeText={(pseudo) => setPseudo(pseudo)}
                                style={{ padding: 10 }}
                            />
                            <Mytextinput
                                placeholder="Mot de Passe"
                                secureTextEntry={true}
                                onChangeText={(password) => setPassword(password)}
                                maxLength={225}
                                keyboardType="numeric"
                                style={{ padding: 10 }}
                            />
                            <Mybutton title="Submit" customClick={register_user} />
                        </KeyboardAvoidingView>
                    </ScrollView>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default LoginScreen;
