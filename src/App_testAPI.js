import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';

export default App = () => {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://newsapi.org/v2/everything?q=bitcoin&apiKey=90ad13e55b2849c68d70f1f356b9be8a')
            .then((response) => response.json())
            .then((json) => setData(json))
            .catch((error) => console.error(error))
            .finally(() => setLoading(false));
    }, []);

    isLoading ? console.log(data) : "false";
    return (
        <></>
    );
};